# Devcontainers Templates - Docker

This project is a [Devcontainer](.devcontainer/) template built against both
`alpine` and `debian` images of the
[Basics template](https://gitlab.com/geekstuff.dev/devcontainers/templates/basics).

It simply adds the [docker vscode feature](https://gitlab.com/geekstuff.dev/devcontainers/features/docker)
on top.

## How it's used

The .devcontainer/ configuration in this project can be used with simple entries
in your own `.devcontainer/devcontainer.json` file where you can further customize
it for example:

```json
{
    "name": "my devcontainer",
    "image": "registry.gitlab.com/geekstuff.dev/devcontainers/templates/docker/alpine/3.17"
}
```

From the example above:

- Image `alpine/3.17` can be changed to `debian/bullseye`.
- Image can also have a tag, `:latest` for the main branch, or `:vX.Y` from
  [tags in this project](https://gitlab.com/geekstuff.dev/devcontainers/templates/docker/container_registry).
- That example also wanted docker client inside the devcontainer.
- See the basics template for info on how to cache VSCode extensions.
